// utils / compileHTML

const {
  compileFile,
} = require('pug');

const readFile = require('./readFile');

const {
  ENV,
  srcDir,
  siteDataFile,
} = require('../configs');

const compileHTML = async (fileSrc) => {
  let tplData = {ENV};

  if (siteDataFile) {
    let tmp = readFile(`${srcDir}/${siteDataFile}`);
    tplData = JSON.parse(tmp);
  }

  let meta = tplData.meta;

  let {
    image = '',
    url = '',
  } = meta;

  if (!image.startsWith('http')) {
    tplData.meta.image = `${url}${image}`;
  }

  let html = compileFile(fileSrc)(tplData);

  return html;
};

module.exports = compileHTML;
